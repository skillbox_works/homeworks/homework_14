#include <iostream>
#include <string>

int main()
{
    std::string S; 
    std::cout << "Input string:";
    std::getline(std::cin, S);
    
    int first = 0, last = S.length()-1;

    std::cout << "S = \"" << S << "\", length = " << (last+1) << "\n" << "First char:" << S[first] << "\nLast char :" << S[last] << "\n";
}
